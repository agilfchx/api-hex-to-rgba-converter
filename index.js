const convert = require("./convert");
const app = require("express")();
const PORT = 8080;

app.get("/api/:hex", (req, res) => {
  res.json({
    color: convert(req.params.hex),
  });
});

app.listen(PORT, () => console.log(`buka di http://localhost:${PORT}/api/`));
