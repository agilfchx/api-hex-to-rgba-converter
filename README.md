## Usage

Open your terminal or console    

    // Clone Repo
    $ git clone https://gitlab.com/agilfchx/api-hex-to-rgba-converter.git
    
    // Open Clone Repo Folder
    $ cd api-hex-to-rgba-converter
    
    // Start ExpressJs with node
    $ node index.js

Then just type `http://localhost:8080/api/{your hex code}`

